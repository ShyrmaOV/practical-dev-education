package com.github.validationPath;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ValidationPath {
    public boolean validate (String path) {
        Pattern p = Pattern.compile("^[a-zA-Z0-9._-]{3,15}$");
        Matcher m = p.matcher(path);
        System.out.println(path);
        return m.matches();
    }
}
