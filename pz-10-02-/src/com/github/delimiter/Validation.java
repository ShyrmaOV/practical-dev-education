package com.github.delimiter;

public class Validation {
    public static String validation(String str) {
        String delimiters = ".,?!:;";
        String result = "";
        boolean prevCharIsDelimiter = false;

        for (int i = 0; i < str.length(); i++) {
            if (delimiters.indexOf(str.charAt(i)) != -1) {
                prevCharIsDelimiter = true;
            } else {
                if (str.charAt(i) != ' ' && prevCharIsDelimiter) {
                    result += " ";
                }
                prevCharIsDelimiter = false;
            }
            result += str.charAt(i);
        }
        System.out.format("input:\t%s\nresult:\t%s\n", str, result);
        return result;
    }
}
