package com.github.delimiter;

import org.junit.Assert;
import org.junit.Test;

public class ValidationTest {

    @Test
    public void validation() {
        String exp = "dfsgh/dfg. sdg, sdf; fg.";
        String act = Validation.validation("dfsgh/dfg. sdg,sdf;fg.");
        Assert.assertEquals(exp, act);
    }
    @Test
    public void validationNull() {
        String act = Validation.validation("dfsgh/dfg. sdg,sdf;fg.");
        Assert.assertNotNull(act);
    }
}