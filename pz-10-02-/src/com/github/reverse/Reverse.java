package com.github.reverse;

public class Reverse {
    public void reverse (String str) {
        String result = str.replace("/", "\\\\");
        System.out.format("input:\t%s\nresult:\t%s", str, result);

    }
}
