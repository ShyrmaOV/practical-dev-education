package com.github.task1;

public class Validation {
    public String validator(String s) {
        s = s.replace(".", ". ");
        s = s.replace(",", ", ");
        s = s.replace("?", "? ");
        s = s.replace("!", "! ");
        s = s.replace(":", ": ");
        s = s.replace(";", "; ");
        return s;
    }

}
