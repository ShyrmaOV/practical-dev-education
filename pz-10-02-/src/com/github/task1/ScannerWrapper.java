package com.github.task1;

import java.util.Scanner;

public class ScannerWrapper {
    public String writeString() {
        Scanner in = new Scanner(System.in);
        String text = in.nextLine();
        return text;
    }
    public int writeInt() {
        Scanner in = new Scanner(System.in);
        int num = in.nextInt();
        return num;
    }
}
