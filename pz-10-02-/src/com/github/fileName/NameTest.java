package com.github.fileName;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class NameTest {

    @Test
    public void getNameFromPath() {
        String exp = "The File Name";
        String act = Name.getNameFromPath("C:\\Hello\\AnotherFolder\\The File Name.PDF");
        assertEquals(exp, act);
    }
    @Test
    public void getNameFromPathNull() {
        String act = Name.getNameFromPath("C:\\Hello\\AnotherFolder\\The File Name.PDF");
        assertNotNull(act);
    }
}