package com.github.fileName;

public class Name {
    public static String getNameFromPath (String path) {
        String fileName = path.substring(path.lastIndexOf("\\") + 1).replaceFirst("[.][^.]+$", "");
        return fileName;
    }
}
