package com.github.array1;

public class Main {
    private static int[] array;
    private static int count;

    public static void main(String[] args) {
        Main.arrayOdd();
        Main.arrayEven();
        Main.randomArray(15, 0, 9);
        Main.arrayFibonache();
        Main.arrayMax(12, -15, 15);

    }
    public static void arrayOdd() {
        array = new int[100];
        count = 1;
        for (int i = 0; i < array.length; i++) {
            array[i] = count++;
            if (array[i] % 2 == 0) {
                System.out.println(array[i]);
            }
        }
    }
    public static void arrayEven() {
        array = new int[100];
        count = 1;
        int[] array = new int[100];
        for (int i = 0; i < array.length; i++) {
            array[i] = count++;
            if (array[i] % 2 == 1) {
                System.out.println(array[i]);
            }
        }
    }
    public static void randomArray(int n, int min, int max) {
        array = new int[n];
        for (int i = 0; i < n; i++) {
            array[i] = min + (int)(Math.random() * (max - min));
            System.out.println(array[i]);
        }
    }
    public static void arrayFibonache() {
        array = new int[20];
        int n0 = 1;
        int n1 = 1;
        int n2;
        System.out.print(n0+" "+n1+" ");
        for(int i = 3; i <= array.length; i++){
            n2=n0+n1;
            System.out.print(n2+" ");
            n0=n1;
            n1=n2;

        }
    }
    public static void arrayMax(int n, int min, int max) {
        array = new int[n];
        int maxEl = array[0];
        for (int i = 0; i < n; i++) {
            array[i] = min + (int) (Math.random() * (max - min));
            if (maxEl < array[i]) {
                maxEl = array[i];
            }
        }
        System.out.println(maxEl);
    }
}
