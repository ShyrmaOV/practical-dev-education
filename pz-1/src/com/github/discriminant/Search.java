package com.github.discriminant;

public class Search {
    private static double discriminant = 0;
    private static double x = 0;
    private static double x1 = 0;
    private static double x2 = 0;

    public static void search(double a, double b, double c){
        discriminant = b*b - (4*a*c);
        if(discriminant > 0) {
            System.out.println("D = " + discriminant);
            x1 = (-b + Math.sqrt(discriminant))/(2*a);
            x2 = (-b - Math.sqrt(discriminant))/(2*a);
            System.out.println("have x1 = " + x1 + " x2 = " + x2);
        } else
        if(discriminant == 0) {
            System.out.println("D = " + discriminant);
            x = -b/(2*a);
            System.out.println("have x = " + x);
        } else {
            System.out.println("D = " + discriminant);
            System.out.println("Вещественных корней нет");
        }

    }
}
