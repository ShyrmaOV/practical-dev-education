package com.github.discriminant;

public class Dialog {
    public static void dialog() {
        System.out.print("Enter a: ");
        double a = ScannerWrapper.getNumber();
        System.out.print("Enter b: ");
        double b = ScannerWrapper.getNumber();
        System.out.print("Enter c: ");
        double c = ScannerWrapper.getNumber();
        System.out.println("-----------------------------\nYour function " + (int)a + " * x^2 + " + (int)b + "* x + " + (int)c + "=0\n-----------------------------");
        Search.search(a, b, c);

    }
}
