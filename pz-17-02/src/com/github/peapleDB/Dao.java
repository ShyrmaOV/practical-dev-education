package com.github.peapleDB;

import java.util.List;

public interface Dao<T> {
    public List<T> getFacultyStudent(String faculty);
    public List<T> getStudentFromFacultyCourse(String faculty, String course);

    public List<T> getStudentUpperBDay(int upperBDay);

    public List<T> getGroupStudent(String faculty);
}
