package com.github.dialog;

import javax.swing.*;
import java.awt.*;

public class OptionDialog {
    public static void main(String[] args) {
//        JOptionPane.showMessageDialog(
//                new JFrame(), "Error 404", "Dialog", JOptionPane.ERROR_MESSAGE
//        );
//
//        JOptionPane.showMessageDialog(
//                new JFrame(), "Do you have question?", "Help", JOptionPane.QUESTION_MESSAGE
//        );
//
//        JOptionPane.showMessageDialog(
//                new JFrame(), "S T O P    B A I T", "Information", JOptionPane.INFORMATION_MESSAGE
//        );
//
//        JOptionPane.showMessageDialog(
//                new JFrame(), "WARNING!!!", "Warning", JOptionPane.WARNING_MESSAGE
//        );
//
//        JOptionPane.showMessageDialog(
//                new JFrame(), "Some text", "Dialog", JOptionPane.PLAIN_MESSAGE
//        );
//
//        String[] options = {"Yes, please", "No, thanks", "Ok, cancel"};
//        JOptionPane.showOptionDialog(
//                new Frame(),"Yes, No or Cancel","A Silly Question",
//                JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE,null, options, options[2]
//        );
//
//        JOptionPane.showConfirmDialog(
//                new Frame(),"Yes or No","Yes or No", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE
//        );
//
//        String[] choose = {"ONE", "TWO", "FREE XD"};
//        JOptionPane.showInputDialog(
//                new JFrame(),"Choose one:","Choose Dialog",
//                JOptionPane.PLAIN_MESSAGE,null, choose, choose[0]
//        );
//
//        JOptionPane.showInputDialog(
//                new JFrame(),"Your text:","Text Field",
//                JOptionPane.DEFAULT_OPTION
//        );

        JColorChooser.showDialog(new JFrame(), "ff", Color.white);

        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setDialogTitle("Выбор директории");
        // Определение режима - только каталог
        fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        int result = fileChooser.showOpenDialog();
        // Если директория выбрана, покажем ее в сообщении
        if (result == JFileChooser.APPROVE_OPTION )
            JOptionPane.showMessageDialog(FileChooserTest.this,
                    fileChooser.getSelectedFile());
    }
}
