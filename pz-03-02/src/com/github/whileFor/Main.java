package com.github.whileFor;

public class Main {
    public static void main(String[] args) {
        Main.while1(0, -10);
        Main.while2(-20, 20);

    }
    public static void while1(int min, int max) {
        int[] array = new int[11];
        int i = 0;
        while (min >= max) {
            array[i] = min;
            System.out.println(array[i]);
            i++;
            min--;

        }
    }
    public static void while2(int min, int max) {
        int[] array = new int[max - min + 1];
        int i = 0;
        while (min <= max) {
            array[i] = min;
            System.out.println(array[i]);
            i++;
            min++;

        }
    }
}
